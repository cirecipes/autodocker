# Run GitLab CI on Docker images built by GitLab CI

Normally, GitLab CI builds using Docker use a fixed Docker image, specified with the `image` setting in the settings of a GitLab CI build job. However, this means that when you need to update the docker image, you have to build the new image and manually push it to a Docker registry, and then update your GitLab CI settings to refer to it.

If you use `gitlab-autodocker` instead, you can let GitLab CI first automatically build the CI Docker image for you, then use that image to build the rest of your CI pipeline. Moreover, `gitlab-autodocker` checks whether your CI Docker context has been updated and only rebuilds the CI Docker image when necessary.

# How to use

* Add `gitlab-autodocker` as a submodule of your git repository. You can put it anywhere, but these instructions assume you put it in `gitlab/autodocker`
* Place your CI Docker context in git. You can put it anywhere, but these instructions assume you put it in `gitlab/build`
* Reference `gitlab-autodocker` by adding the following to the top of your `.gitlab-ci.yml`:

```
include:
  - project: airbornemint/gitlab-autodocker
    file: "gitlab-autodocker.yml"
```

* Add `autodocker` as the first stage in your `.gitlab-ci.yml`:

```
stages:
  - autodocker
  - …
```

* Configure the name (not the full path) of your CI Docker context:

```
variables:
  AUTODOCKER_CONTEXT_NAME: build
```

* Add `autodocker` job to your `.gitlab-ci.yml`:

```
autodocker:
  extends: .autodocker
```

If you placed the `gitlab-autodocker` submodule at a path other than `gitlab/autodocker`, then you also need to specify that in the configuration of the `autodocker-build` job, using the `AUTODOCKER_PATH` variable; for example: `AUTODOCKER_PATH: builds/autodocker`.

If your CI Docker context is at a path other than `gitlab/${AUTODOCKER_CONTEXT_NAME}`, then you also need to specify that in the configuration of the `autodocker-build` job, using the `AUTODOCKER_CONTEXT` variable; for example: `AUTODOCKER_CONTEXT: builds/${AUTODOCKER_CONTEXT_NAME}`.

7. Configure the Docker image for your CI jobs by making each of your jobs extend `.use-autodocker`

```
your-job:
  extends: .use-autodocker
```

# Advanced: different contexts for different jobs

If you are using different contexts for different jobs, you will need to:

1. Follow steps 1-4 above
2. In step 5, add the `AUTODOCKER_CONTEXT_NAME` variable to each job separately
3. In step 6, add multiple jobs, all extending `.autodocker`, with different values for `AUTODOCKER_CONTEXT`
4. Follow step 7 above

# What you will see

What you are going to see after you make these changes is:

 * Every pipeline will have an `autodocker` stage
 * During the execution of this stage, Docker will build a new image for your CI if your CI Docker context has changed; otherwise it will pull an existing image.
 * These images are stored in your GitLab Docker image registry under the tag `autodocker-$context:rev-$rev`, where `$context` is the name of your CI context, and `$rev` is the hash of the commit when the context was updated. A new one of these images will be created every time you modify the context. If you delete them, they will be regenerated next time they are needed for a build (which, for most of them, will probably be never, since re-running old builds is uncommon).
 * Either way, Docker will then push the CI image under the new tag `autodocker-commit-$context:rev-$commit`, where `$context` is the name of your CI context and `$rev` is the hash of the commit that is being built by CI. These images are also stored in your GitLab Docker image registry, and they are used for a single build. If you delete them, they will only be regenerated if you subsequently rerun the pipeline for that build. Note that these images are just re-tags, so they don't take up any space in your registry; deleting them won't get you any space back.
 * Other stages in your pipeline will pull these images as needed
